#!/usr/bin/env python
try:  # GTK3
    from gi.repository import Gtk
    from gi.repository import GObject
except ImportError:  # GTK2
    import Gtk as Gtk
    import GObject as GObject

from axis_widget import AxisWidget, _set_xy
from joystick import Joystick

rows = 12
cols = 12

window = Gtk.Window()
window.set_title('Stresstest {0}x{1}'.format(cols, rows))
window.set_size_request(500, 500)
table = Gtk.Table(rows, cols, True)


def place_widget(table, x, y, w, h):
    axis_widget = AxisWidget(False)
    joy = Joystick('simulate')
    joy.connect('axis', _set_xy(axis_widget))
    table.attach(axis_widget, x, x + w, y, y + h)

for col in range(0, cols, 4):
    for row in range(0, rows, 4):
        if col == 4 and row == 4:
            continue
        place_widget(table, col,   row,   2, 2)
        place_widget(table, col + 2, row + 2, 2, 2)

for col in range(0, cols, 4):
    for row in range(0, rows, 4):
        if col == 4 and row == 4:
            continue
        place_widget(table, col + 2, row,   1, 1)
        place_widget(table, col + 3, row,   1, 1)
        place_widget(table, col + 2, row + 1, 1, 1)
        place_widget(table, col + 3, row + 1, 1, 1)

        place_widget(table, col,   row + 2, 1, 1)
        place_widget(table, col,   row + 3, 1, 1)
        place_widget(table, col + 1, row + 2, 1, 1)
        place_widget(table, col + 1, row + 3, 1, 1)


place_widget(table, 4, 4, 4, 4)

window.add(table)
window.connect('destroy', Gtk.main_quit)
window.show_all()

Gtk.main()
