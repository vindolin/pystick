#!/usr/bin/env python
from __future__ import print_function

try:  # GTK3
    from gi.repository import GObject
except ImportError:  # GTK2
    import GObject as GObject

import socket
from joystick import *


class JoystickServer():

    def __init__(self, host, port):
        self.clients = set()
        sock = socket.socket()
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(1)
        GObject.io_add_watch(sock, GObject.IO_IN, self.sock_listener)

    def on_joystick_data(self, emitter, time, value, type_, number):
        msg = struct.pack(EVENT_FORMAT, time, value, type_, number)

        for client in self.clients:
            totalsent = 0
            try:
                while totalsent < EVENT_SIZE:
                    sent = client.send(msg[totalsent:])
                    if sent == 0:
                        raise RuntimeError("socket connection broken")
                    totalsent = totalsent + sent
            except socket.error:
                continue

    def sock_listener(self, sock, *args):
        conn, addr = sock.accept()
        self.clients.add(conn)
        print('connected')
        GObject.io_add_watch(conn, GObject.IO_IN, self.sock_handler)
        return True

    def sock_handler(self, conn, *args):
        try:
            line = conn.recv(4096)
            if not len(line):
                self.clients.remove(conn)
                print("Connection closed.")
                return False
            else:
                return True

        except socket.error:
            print("Connection left the building...")
            return False


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Connect to a joystick and stream the data to a running joystick.py instance.')
    parser.add_argument('device', help='the device to bind to e.g. /dev/input/js0 (default: simulate)', type=str, default='simulate', nargs='?')
    parser.add_argument('interface', help='interface:port eg: 0.0.0.0:9119', type=str, default='0.0.0.0:9119', nargs='?')
    parser.add_argument('--dead_spot', help='size of the dead spot in the center where the values snap to 0', type=float, default=0.1)
    parser.add_argument('--treshold', help='how much motion must be made to trigger an event', type=float, default=0.05)
    args = parser.parse_args()
    locals().update(vars(args))

    interface, port = interface.split(':')

    '''connect to a joystick device'''
    joy = Joystick(device, dead_spot=dead_spot, treshold=treshold)

    server = JoystickServer('0.0.0.0', 9119)
    joy.connect('joy_data', server.on_joystick_data)

    GObject.MainLoop().run()
