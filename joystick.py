#!/usr/bin/env python
from __future__ import print_function

try:  # GTK3
    from gi.repository import GObject
except ImportError:  # GTK2
    import gobject as GObject

import struct
import socket
from fcntl import ioctl
from sys import exit
from motion_simulator import Simulator

EVENT_FORMAT = "IhBB"
EVENT_SIZE = struct.calcsize(EVENT_FORMAT)

TYPE_BUTTON = 1
TYPE_AXIS = 2


def get_joystick_info(device):

    device = open(device, 'r')

    axis_count = struct.unpack("b", ioctl(device, 0x80016a11, " "))[0]
    button_count = struct.unpack("b", ioctl(device, 0x80016a12, " "))[0]
    device_name = struct.unpack("1024s", ioctl(device, 0x81006a13, " " * 1024))[0].rstrip().strip("\x00")

    return {
        'device_name': device_name,
        'button_count': button_count,
        'axis_count': axis_count,
    }


class JoystickBase(GObject.GObject):

    __gsignals__ = {
        'joy_data': (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, (GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_INT)),
        'button': (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, (GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_INT)),
        'axis': (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, (GObject.TYPE_INT, GObject.TYPE_INT, GObject.TYPE_FLOAT)),
    }

    def __init__(self, device, dead_spot=0, treshold=0):
        GObject.GObject.__init__(self)

        self.dead_spot = dead_spot
        self.treshold = treshold
        self.button_values = {}
        self.axis_values = {}

        self._connect(device)

    def _emit_signals(self, time, value, type_, number):
        self.emit('joy_data', time, value, type_, number)

        if type_ == TYPE_BUTTON:
            self.button_values[number] = value
            self.emit('button', time, number, value)

        elif type_ == TYPE_AXIS:
            self.button_values[number] = value
            value /= 32768.0

            '''snap to 0 inside the dead spot'''
            if abs(value) <= self.dead_spot:
                value = 0

            last_value = self.axis_values.setdefault(number, 0)

            '''not enough change?'''
            if value == last_value or abs(last_value - value) < self.treshold:
                return

            '''remember last value'''
            self.axis_values[number] = value

            self.emit('axis', time, number, value)

        return False

    def get_button(self, number):
        return self.button_values.setdefault(number, 0)

    def get_axis(self, number):
        return self.axis_values.setdefault(number, 0)


class JoystickDevice(JoystickBase):

    def _connect(self, device):
        try:
            print(str(get_joystick_info(device)) + '\n')
            self.joy_device = open(device, 'r')
        except IOError:
            exit('Error opening joystick device')

        GObject.io_add_watch(self.joy_device, GObject.IO_IN, self._read_handler)

    def _read_handler(self, *args):
        read_event = self.joy_device.read(EVENT_SIZE)
        time, value, type_, number = struct.unpack(EVENT_FORMAT, read_event)
        self._emit_signals(time, value, type_, number)

        return True


class JoystickSim(JoystickBase):

    def _connect(self, device):
        GObject.timeout_add(25, Simulator(self._read_handler_0))
        GObject.timeout_add(25, Simulator(self._read_handler_1))

    def _read_handler_0(self, xy):
        self._emit_signals(0, xy[0] * 32768.0, 2, 0)
        self._emit_signals(0, xy[1] * 32768.0, 2, 1)
        return True

    def _read_handler_1(self, xy):
        self._emit_signals(0, xy[0] * 32768.0, 2, 2)
        self._emit_signals(0, xy[1] * 32768.0, 2, 3)
        return True


class JoystickSocket(JoystickBase):

    def _connect(self, device):
        try:
            conn = socket.create_connection(device.split(':'))
            GObject.io_add_watch(conn, GObject.IO_IN, self._read_handler)
        except:
            print('error opening socket')

    def _read_handler(self, conn, *args):
        try:
            msg = ''
            while len(msg) < EVENT_SIZE:
                chunk = conn.recv(EVENT_SIZE - len(msg))
                if chunk == '':
                    raise RuntimeError("socket connection broken")
                msg = msg + chunk

            time, value, type_, number = struct.unpack(EVENT_FORMAT, msg)

            self._emit_signals(time, value, type_, number)
        except:
            raise

        return True


def Joystick(device, dead_spot=0, treshold=0):

        if ':' in device:
            '''connect to a network socket'''
            return JoystickSocket(device, dead_spot=dead_spot, treshold=treshold)
        elif device == 'simulate':
            '''simulate random movement'''
            return JoystickSim(None, dead_spot=0, treshold=0)
        else:
            '''connect to a joystick device'''
            return JoystickDevice(device, dead_spot=dead_spot, treshold=treshold)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Connect to a joystick and and emit the GObject signals button and axis.')
    parser.add_argument('device', help='the device to bind to e.g. /dev/input/js0 (default: simulate)', type=str, default='simulate', nargs='?')
    parser.add_argument('--dead_spot', help='size of the dead spot in the center where the values snap to 0', type=float, default=0.1)
    parser.add_argument('--treshold', help='how much motion must be made to trigger an event', type=float, default=0.05)
    args = parser.parse_args()
    locals().update(vars(args))  # imports device, dead_spot, treshold

    joy = Joystick(device, dead_spot=dead_spot, treshold=treshold)

    def on_button(emitter, *args):
        print("button: {1: >2} value: {2:>13}".format(*args))

    def on_axis(emitter, *args):
        print("axis:   {1: >2} value: {2:>13.10f}".format(*args))

    joy.connect('button', on_button)
    joy.connect('axis', on_axis)

    GObject.MainLoop().run()
