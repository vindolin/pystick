#!/usr/bin/env python
from __future__ import print_function

try:
    from gi.repository import Gtk
    from gi.repository import Gdk
    from gi.repository import GObject
    gtk_version = 3
except ImportError:
    import gtk as Gtk
    from gtk import gdk as Gdk
    import gobject as GObject
    gtk_version = 2

import cairo
import sys
from math import pi, ceil


class AxisWidget(Gtk.DrawingArea):

    __gtype_name__ = 'AxisWidget'

    nub_size_factor = 0.08
    outer_radius_factor = 0.4
    padding_factor = 0.03

    def __init__(self, auto_tooltip=True):
        super(AxisWidget, self).__init__()
        Gtk.DrawingArea.__init__(self)

        self._x_value = 0
        self._y_value = 0

        self.auto_tooltip = auto_tooltip

        self._width = 0
        self._height = 0

        self._x_center = 0
        self._y_center = 0

        self._outer_r = 0
        self._min_dimension = 0
        self._padding = 0

        self._nub_r = None
        self._nub_x = 0
        self._nub_y = 0

        self._background_surface = None
        self._nub_surface = None

        self._last_rect = None

        if self.auto_tooltip:
            self._set_tooltip()

        if gtk_version == 2:
            self.connect('expose-event', self._on_draw)
        else:
            self.connect('draw', self._on_draw)
        self.connect('realize', self._on_realize)
        self.connect('size-allocate', self._on_size_allocate)

    @property
    def x(self):
        return self._x_value

    @x.setter
    def x(self, value):
        self._x_value = value
        self._update()

    @property
    def y(self):
        return self._x_value

    @y.setter
    def y(self, value):
        self._y_value = value
        self._update()

    @property
    def xy(self):
        return (self._x_value, self._y_value)

    @xy.setter
    def xy(self, value):
        self.x = value[0]
        self.y = value[1]
        self._update()

    def _set_tooltip(self):
        self.set_tooltip_text("x: {0:.4f}\ny: {1:.4f}".format(self._x_value, self._y_value))

    def _rounded_rect(self, ctx, x, y, w, h, r):
        ctx.move_to(x + r, y)
        ctx.line_to(x + w - r, y)
        ctx.curve_to(x + w, y, x + w, y, x + w, y + r)
        ctx.line_to(x + w, y + h - r)
        ctx.curve_to(x + w, y + h, x + w, y + h, x + w - r, y + h)
        ctx.line_to(x + r, y + h)
        ctx.curve_to(x, y + h, x, y + h, x, y + h - r)
        ctx.line_to(x, y + r)
        ctx.curve_to(x, y, x, y, x + r, y)

    def _prepare_background_surface(self):
        '''background surface'''
        self._background_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self._width, self._height)
        ctx = cairo.Context(self._background_surface)

        ctx.set_line_width(self._min_dimension / 30)
        ctx.arc(self._x_center, self._y_center, self._outer_r, 0, 2 * pi)

        ctx.set_source_rgb(0, 0, 0)
        ctx.stroke()

        # gradient
        pat = cairo.RadialGradient(
            self._x_center,
            self._y_center,
            self._outer_r,
            self._x_center - self._outer_r / 3,
            self._y_center - self._outer_r / 3,
            self._outer_r / 4
        )
        pat.add_color_stop_rgb(1, 1, 1, 1)
        pat.add_color_stop_rgb(0.2, 0.8, 0.8, 0.8)
        pat.add_color_stop_rgb(0, 0.6, 0.6, 0.6)

        ctx.set_source(pat)
        ctx.arc(self._x_center, self._y_center, self._outer_r, 0, 2 * pi)
        ctx.fill()

        # cross
        ctx.set_line_width(1)
        ctx.set_source_rgba(0, 0, 0, 0.1)

        padding_x = (self._width - (self._outer_r * 2)) / 2
        padding_y = (self._height - (self._outer_r * 2)) / 2

        ctx.move_to(self._x_center, padding_y)
        ctx.line_to(self._x_center, self._height - padding_y)

        ctx.move_to(padding_x, self._y_center)
        ctx.line_to(self._width - padding_x, self._y_center)
        ctx.stroke()

        ctx.set_source_rgba(0, 0, 0, 0.05)
        self._rounded_rect(
            ctx,
            self._x_center - self._outer_r / 2,
            self._y_center - self._outer_r / 2,
            self._outer_r,
            self._outer_r,
            self._nub_r
        )
        ctx.stroke()

    def _prepare_nub_surface(self):
        '''surface for the joystick nub'''

        self._nub_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(ceil(self._nub_d * 1.2)), int(ceil(self._nub_d * 1.2)))  # +0.2 for the extra pixels from fill
        nub_ctx = cairo.Context(self._nub_surface)

        # joystick position circle
        nub_ctx.arc(
            self._nub_r,
            self._nub_r,
            self._nub_r,
            0,
            2 * pi
        )
        nub_ctx.set_source_rgb(0, 0, 0)
        nub_ctx.fill()

    def _on_realize(self, widget):
        #print('realize')
        pass

    def _on_size_allocate(self, widget, event):
        self._width = event.width
        self._height = event.height

        self._x_center = self._width / 2
        self._y_center = self._height / 2

        self._min_dimension = min(self._width, self._height)

        self._padding = self._min_dimension * self.padding_factor
        self._outer_r = self._min_dimension * self.outer_radius_factor
        self._nub_r = self._min_dimension * self.nub_size_factor
        self._nub_d = self._nub_r * 2

        self._prepare_nub_surface()
        self._prepare_background_surface()
        self._update()

    def _update(self):
        self._nub_x = self._x_center + (self._x_value * self._outer_r / 2) - self._nub_r
        self._nub_y = self._y_center + (self._y_value * self._outer_r / 2) - self._nub_r
        rect = (int(self._nub_x), int(self._nub_y), self._nub_surface.get_width(), self._nub_surface.get_height())
        if self._last_rect:
            self.queue_draw_area(*self._last_rect)
        self.queue_draw_area(*rect)
        self._last_rect = rect

    def _on_draw(self, widget, ctx):
        if gtk_version == 2:
            event = ctx
            ctx = widget.window.cairo_create()
            ctx.rectangle(event.area.x, event.area.y, event.area.width, event.area.height)
            ctx.clip()

        #TODO no event.area in GTK3.. clip()?

        ctx.set_source_surface(self._background_surface, 0, 0)
        ctx.paint()

        ctx.set_source_surface(
            self._nub_surface,
            self._nub_x,
            self._nub_y
        )

        ctx.paint()

        if self.auto_tooltip:
            self._set_tooltip()

        return False


def _set_xy(axis_widget):
    def call(joystick, time, number, value):
        if number == 0:
            axis_widget.x = value
        elif number == 1:
            axis_widget.y = value
    return call

if __name__ == "__main__":
    from os import path
    from joystick import Joystick

    def on_key_press_event(widget, event):
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == 'Escape':
            GObject.MainLoop().quit()
            sys.exit()

    window = Gtk.Window()
    window.set_title(path.basename(__file__))
    axis_widget = AxisWidget(False)
    joy = Joystick('simulate')
    joy.connect('axis', _set_xy(axis_widget))
    window.add(axis_widget)
    window.connect('destroy', Gtk.main_quit)
    window.connect('key-press-event', on_key_press_event)
    window.show_all()

    Gtk.main()
