from random import random
from math import pi, sin, cos


class Simulator():
    '''simulate random joystick movements'''
    def __init__(self, callback):
        self.callback = callback

        self.osci1 = 0
        self.osci2 = 0
        self.twopi = pi * 2
        self.add1 = 0.05 + (0.1 * random())
        self.add2 = 0.05 + (0.1 * random())

    def __call__(self):
        self.osci1 += self.add1
        self.osci2 += self.add2

        self.osci1 %= self.twopi
        self.osci2 %= self.twopi

        self.callback((sin(self.osci1), cos(self.osci2)))
        return True

if __name__ == "__main__":
    try:  # GTK3
        from gi.repository import GObject
    except ImportError:  # GTK2
        import gobject as GObject

    def callback(args):
        print('x:%s y:%s' % args)

    GObject.timeout_add(25, Simulator(callback))
    GObject.MainLoop().run()
