# pystick

Bits and pieces from a ROV project.

Python Joystick GTK

*This code is work in progress!*


![screenshot](https://bitbucket.org/vindolin/pystick/raw/2f3be5c39980/screenshot.jpg)


## [joystick.py](https://bitbucket.org/vindolin/pystick/src/master/joystick.py)

Reads joystick data from the device and emits gobject signals.  
Alternatively connect to a running joystick server instance.

## [joystick_server.py](https://bitbucket.org/vindolin/pystick/src/master/joystick_server.py)

stream the joystick data to a socket

## [axis_widget.py](https://bitbucket.org/vindolin/pystick/src/master/axis_widget.py)

Custom PyGTK widget.
See joy_client_ui.py for usage.


## [joy_client_ui.py](https://bitbucket.org/vindolin/pystick/src/master/joy_client_ui.py)

Sample GUI which shows the axis_widget in action.  
When started without parameters it uses /dev/input/js0 as the joystick device.  
The joystick device to be used can be supplied:

    python joy_client_ui.py '/dev/input/js1'
    python joy_client_ui.py 'localhost:9119' # needs a running joystick_server.py instance.


## [stresstest1.py](https://bitbucket.org/vindolin/pystick/src/master/stresstest1.py) / [stresstest2.py](https://bitbucket.org/vindolin/pystick/src/master/stresstest2.py)

Draw 10x10 axis_widgets with random movement from motion_simulator.py
