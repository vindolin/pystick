#!/usr/bin/env python
try:  # GTK3
    from gi.repository import Gtk
except ImportError:  # GTK2
    import gtk as Gtk

from axis_widget import AxisWidget, _set_xy
from joystick import Joystick

rows = 10
cols = 10

window = Gtk.Window()
window.set_title('Stresstest {0}x{1}'.format(cols, rows))
window.set_size_request(500, 500)
table = Gtk.Table(rows, cols, False)

for col in range(cols):
    for row in range(rows):
        axis_widget = AxisWidget(False)
        joy = Joystick('simulate')
        joy.connect('axis', _set_xy(axis_widget))
        table.attach(axis_widget, col, col + 1, row, row + 1)

window.add(table)
window.connect('destroy', Gtk.main_quit)
window.show_all()

Gtk.main()
