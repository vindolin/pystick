#!/usr/bin/env python
from __future__ import print_function

import sys

try:  # GTK3
    from gi.repository import Gtk
    from gi.repository import Gdk
    from gi.repository import GObject
except ImportError:  # GTK2
    import gtk as Gtk
    from gtk import gdk as Gdk
    import gobject as GObject

from axis_widget import AxisWidget
from axis_mapping import axis_mapping


class JoyUI(GObject.GObject):

    gladefile = 'joy_client_ui.glade'

    def __init__(self):
        GObject.GObject.__init__(self)

        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)

        self.window = self.g("window1")

        self.window.connect("destroy", self.destroy)
        self.window.set_border_width(10)

        self.axis_widget1 = AxisWidget()
        self.axis_widget1.set_name('axis1')
        self.axis_widget1.show()
        self.g('alignment1').add(self.axis_widget1)

        self.axis_widget2 = AxisWidget()
        self.axis_widget2.set_name('axis2')
        self.axis_widget2.show()

        self.axis_mapping = {
            0: (self.axis_widget1, axis_mapping[0]),
            1: (self.axis_widget1, axis_mapping[1]),
            3: (self.axis_widget2, axis_mapping[2]),
            2: (self.axis_widget2, axis_mapping[3]),
        }

        self.g('alignment2').add(self.axis_widget2)
        self.statusbar = self.g('statusbar1')

        try:  # GTK3
            self.window.set_position(Gtk.WIN_POS_CENTER)
        except AttributeError:  # GTK2
            self.window.set_position(Gtk.WindowPosition.CENTER)

        self.window.connect('key-press-event', self.on_key_press_event)
        self.window.show()

    def g(self, object_name):
        return self.builder.get_object(object_name)

    def destroy(self, widget, data=None):
        GObject.MainLoop().quit()
        sys.exit()

    def on_joy_button(self, joystick, time, number, value):
        self.statusbar.push(
            self.statusbar.get_context_id('number'),
            '  '.join([str(button_number) for button_number, button_value in joystick.button_values.items() if button_value == 1])
        )

    def on_joy_axis(self, joystick, time, number, value):
        if number in self.axis_mapping:
            self.axis_mapping[number][0].__setattr__(self.axis_mapping[number][1], value)

    def on_key_press_event(self, widget, event):
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == 'Escape':
            GObject.MainLoop().quit()
            sys.exit()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Example UI that shows an axis_widget in action.')
    parser.add_argument('device', help='the device to bind to e.g. /dev/input/js0 (default: simulate)', type=str, default='simulate', nargs='?')
    parser.add_argument('--dead_spot', help='size of the dead spot in the center where the values snap to 0', type=float, default=0.1)
    parser.add_argument('--treshold', help='how much motion must be made to trigger an event', type=float, default=0.05)
    args = parser.parse_args()
    locals().update(vars(args))

    from joystick import Joystick
    joy = Joystick(device, dead_spot=dead_spot, treshold=treshold)

    joy_ui = JoyUI()

    joy.connect('axis', joy_ui.on_joy_axis)
    joy.connect('button', joy_ui.on_joy_button)

    Gtk.main()
